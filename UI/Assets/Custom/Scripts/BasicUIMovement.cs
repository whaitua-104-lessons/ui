﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BasicUIMovement : MonoBehaviour {
	[Serializable]
	public class ButtonSet {
		public Button left;
		public Button right;
	}

	public ButtonSet buttons;

	void Awake(){
	
		buttons.left.onClick.AddListener(() => {Player.instance.Move(Vector3.left);});
		buttons.right.onClick.AddListener(() => {Player.instance.Move(Vector3.right);});

	}



}
