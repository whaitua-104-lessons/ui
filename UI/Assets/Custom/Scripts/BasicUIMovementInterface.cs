﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BasicUIMovementInterface : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public enum Direction
	{
		Left,
		Right
	}

	public Direction direction = Direction.Left;
	private float power;


	void Update() {
	
		if (Mathf.Abs (power) > 0) {
			Player.instance.Move (Vector3.right * power);

		}
	}
	
	public void OnPointerEnter(PointerEventData eventData){
	

		power = direction == Direction.Left ? -1 : 1;
	

	}

	public void OnPointerExit(PointerEventData eventData){
		
		power = 0;
		Player.instance.Move (Vector3.right * power);
	}

}