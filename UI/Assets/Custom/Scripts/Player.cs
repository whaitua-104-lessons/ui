﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public static Player instance;

	public float speed = 3.0f;

	void Awake() {
		instance = this;
	}

	public void Move(Vector3 _direction){

		transform.position += _direction * speed * Time.deltaTime;

	}
}
