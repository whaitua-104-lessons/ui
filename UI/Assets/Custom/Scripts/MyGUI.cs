﻿using System;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class MyGUI : MonoBehaviour {





	public Transform target;
	public float speed = 3.0f;
	public ButtonSet guiButtons = new ButtonSet();


	[Serializable]
	public class ButtonSet {
		public Rect getRectLeft = new Rect (5, 25, 100, 20);
		public Rect getRectRight = new Rect(5, 50, 100, 20);
	}


	void OnGUI() {



		GUI.Box (new Rect(5, 5, 100, 20), "Looook");


		if(GUI.RepeatButton(guiButtons.getRectLeft, "Left")) {
			//target.position += Vector3.left * speed * Time.deltaTime;
			Move(Vector3.left);
		} 

		if(GUI.RepeatButton(guiButtons.getRectRight, "Right")) {
			//target.position += Vector3.right * speed * Time.deltaTime;
			Move(Vector3.right);
		} 
	}

	void Move(Vector3 _direction){

		target.position += _direction * speed * Time.deltaTime;

	}
}


//////DUN DUN DUN DUN DUN DUN SEPHIROOOTH!